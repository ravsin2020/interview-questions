package com.jps.ago.com;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

// Class name should be "Source",
// otherwise solution won't be accepted
/**
 * 
 * @author Ravi
 * 
4
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
6
RIGHT
DOWN
RIGHT
DOWN
LEFT
UP

 *
 */
public class Source {
    public static void main(String[] args) {
        System.out.println("Enter Input:");
        Scanner in = new Scanner(System.in);
        List<String> list= new ArrayList<>();
        // Declare the variable
        int a;
        while(in.hasNextLine()){
            list.add(in.nextLine());
        }
        in.close();
        int size= Integer.parseInt(list.get(0));
        String strVals="";
        for(int i=1; i<size ;i++){
            strVals = strVals + " " + list.get(i);
        }
        String[] elements= strVals.split(" "); int index=0;
        int[][] matrix= new int[size][size];
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                matrix[i][i]=Integer.parseInt(elements[index]);
                index++;
            }
        }
        int reqMoves=Integer.parseInt(list.get(size+1)); index= size+2;
        int i=0; int j=0;
        for(int x=0; x<reqMoves;x++){
            switch(list.get(index+x)){
                case "RIGHT":
                {
                    if(i+1 <size){
                        i++;
                    }
                }
                break;
                case "LEFT":
                {
                    if(i-1 >=0){
                        i--;
                    }
                }
                break;
                case "DOWN":
                {
                    if(j+1 <size){
                        j++;
                    }
                }
                break;
                case "UP":
                {
                    if(j-1 >=0){
                        j--;
                    }
                }
                break;
                
            }
        }
        // Read the variable from STDIN
        a = matrix[i][j];
        
        // Output the variable to STDOUT
        System.out.println(a);
   }
}

