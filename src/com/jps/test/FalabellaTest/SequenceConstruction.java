package com.jps.test.FalabellaTest;

import java.util.ArrayList;
import java.util.List;

public class SequenceConstruction {
	public static void main(String[] args) {
		System.out.println(fndSequence(5, 9, 10));
		System.out.println(fndSequence(5, 4, 11));
	}

	public static List<Integer> fndSequence(int n, int lo, int hi) {
		List<Integer> list= new ArrayList<>(); boolean isInc=true;
		int start = (hi+2-lo)>=n? hi-1 : lo;
		for (int i = 0; i < n; i++) {
			list.add(start);
			if(isInc && start<hi){
				start++;
			}else{
				start--;
				isInc = false;
			}
		}
		if (list.get(list.size()-1) < lo) {
			list = new ArrayList();
			list.add(-1);
		}
		return list;
	}
}
