package com.jps.test.jpmorgan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
/**
 * 
 * @author Ravikant.singh
 * 
 * Checkerboard Colors
Programming challenge description:
Given a square's location on a checkerboard, write a program to determine the color of the square.

Each square can be one of two colors: Black or White. The square in the bottom left is always Black and the size of the checkerboard is 8x8.

For any given square, all its adjacent squares have a different color. In other words, any two squares have different colors if they share an edge between them.

Columns are designated by characters a, b, c, d, e, f, g, h from left to right. Rows are designated by numbers 1, 2, 3, 4, 5, 6, 7, 8 from bottom to top. The address of a single square is a concatenation of the character and integer values.

See the attachments tab for an image of the checkerboard.

Input:
A string with the alpha-numeric address of the square. For example:

f7
Output:
Print the color of the square: Black or White.

Print Error if the address is invalid.

Test 1
Test Input
Download Test 1 Input
f7
Expected Output
Download Test 1 Input
White
Test 2
Test Input
Download Test 2 Input
a1
Expected Output
Download Test 2 Input
Black
Test 3
Test Input
Download Test 3 Input
bh9
Expected Output
Download Test 3 Input
Error
Test 4
Test Input
Download Test 4 Input
d5
Expected Output
Download Test 4 Input
White
 *
 */
public class Main {
	  /**
	   * Iterate through each line of input.
	   */
	  public static void main(String[] args) throws IOException {
		  System.out.println('7');
		  String[][] checkBox= new String[8][8];
		  String colour="Back";
		  for(int i=0;i<checkBox.length;i++){
			  checkBox[i][0]=colour;
			  colour= colour=="Black" ? "White": "Black";
		  }
		  colour = null;
		  for (int i = 0; i < checkBox.length; i++) {
			for (int j = 0; j < checkBox.length; j++) {
				colour = colour==null ? checkBox[i][j] : colour;
				checkBox[i][j]=colour;
				colour= colour=="Black" ? "White": "Black";
			}
		}
		  // val of a =97
		  
	    InputStreamReader reader = new InputStreamReader(System.in, StandardCharsets.UTF_8);
	    BufferedReader in = new BufferedReader(reader);
	    String line;
	    while ((line = in.readLine()) != null) {
	    	if(line.charAt(0)-97 > 7 || line.charAt(0)-97 < 0 || line.charAt(1)>'8' || line.charAt(0)-0<'1'){
	    		System.out.print("Error");
	    	}else{
	    		System.out.println(checkBox[line.charAt(0)-97][(Character.getNumericValue(line.charAt(1))-1)]);
	    	}
	    /*  if(line.charAt(0) == 'a' || line.charAt(0) == 'c' || line.charAt(0) == 'e' || line.charAt(0) == 'g'){
	        if(line.charAt(1) == '1' || line.charAt(1) == '3' || line.charAt(1) == '5' || line.charAt(1) == '7'){
	           System.out.print("Black");
	        }else if(line.charAt(1) == '2' || line.charAt(1) == '4' || line.charAt(1) == '6' || line.charAt(1) == '8'){
	         System.out.print("White");
	        }else{
	          System.out.print("Error");
	        }
	      }else if(line.charAt(0)== 'b' || line.charAt(0)== 'd' || line.charAt(0)== 'f' || line.charAt(0)== 'h'){
	         if(line.charAt(1) == '1' || line.charAt(1) == '3' || line.charAt(1) == '5' || line.charAt(1) == '7'){
	           System.out.print("White");
	        }else if(line.charAt(1) == '2' || line.charAt(1) == '4' || line.charAt(1) == '6' || line.charAt(1) == '8'){
	         System.out.print("Black");
	        }else{
	          System.out.print("Error");
	        }
	      }else{
	        System.out.print("Error");
	      }*/
	    }
	  }
	}