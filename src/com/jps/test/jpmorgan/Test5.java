package com.jps.test.jpmorgan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/**
 * 
 * @author Ravi
 *
 *
 *
 *Single-Source Shortest Path
Programming challenge description:
Write a program to find the shortest paths from one source vertex to all other reachable vertices in the given undirected graph.

Edges of the graph have no weights and the length of the path is the number of edges between two vertices.

The graph can be disconnected, i.e. consisting of multiple pieces. If a vertex is not reachable from the source vertex the path between them is equal to infinity.

Every vertex in the graph has a unique string name consisting of alphanumeric characters (UTF-8).

Input:
The first line contains the name of the source vertex.

The second line has a list of edges. Every edge is described as a comma-delimited pair of names of adjacent vertices. Edges are separated with a semicolon in the list. For example:

A
A,B;B,C;A,C;B,D;C,D;D,E;F,G;G,H;F,H
See the attachments tab for a diagram of this example

Output:
Each line contains a space-delimited vertex name and a positive integer number, which is equal to the length of the shortest path from the source vertex.

If there is no path to some vertex from the source, print INF instead of the integer length.

Sort lines in the alphabetical order by vertex name and do not include the source vertex in the output. For example:

B 1
C 1
D 2
E 3
F INF
G INF
H INF

Test 1
Test Input
Download Test 1 Input
A
A,B;B,C;A,C;B,D;C,D;D,E;F,G;G,H;F,H;
Expected Output
Download Test 1 Input
B 1
C 1
D 2
E 3
F INF
G INF
H INF
Test 2
Test Input
Download Test 2 Input
grass
grasshopper,hawk;lizard,hawk;grass,grasshopper;grasshopper,lizard;mouse,hawk;snake,hawk;rabbit,hawk;mouse,snake;grass,rabbit;grass,mouse
Expected Output
Download Test 2 Input
grasshopper 1
hawk 2
lizard 2
mouse 1
rabbit 1
snake 2
Test 3
Test Input
Download Test 3 Input
1
106,212;100,200;101,10;209,104;102,11;c,d;202,203;107,214;100,10;212,106;212,213;103,207;d,c;203,101;213,106;205,204;208,209;b,c;201,100;1,11;a,b;208,104;103,206;209,210;11,103;b,a;12,105;214,213;202,201;206,207;104,208;200,100;102,205;215,200;104,209;213,212;c,b;207,206;214,215;101,203;13,106;206,103;211,210;210,211;10,1;105,211;201,200;d,a;212,211;214,107;12,1;210,105;106,213;103,11;1,10;200,215;10,101;102,204;100,201;209,208;203,202;206,205;204,205;1,12;205,206;211,105;105,12;210,209;204,102;215,107;13,1;13,107;213,214;215,214;202,101;207,103;107,215;201,202;205,102;106,13;203,204;10,100;a,d;11,1;12,104;208,207;107,13;11,102;104,12;207,208;204,203;105,210;1,13;211,212;101,202;200,201
Expected Output
Download Test 3 Input
10 1
100 2
101 2
102 2
103 2
104 2
105 2
106 2
107 2
11 1
12 1
13 1
200 3
201 3
202 3
203 3
204 3
205 3
206 3
207 3
208 3
209 3
210 3
211 3
212 3
213 3
214 3
215 3
a INF
b INF
c INF
d INF
Test 4
Test Input
Download Test 4 Input
broccoli
carrot,potato;onion,potato;carrot,cucumber;potato,carrot;potato,tomato;carrot,tomato;tomato,carrot;cucumber,carrot;cucumber,onion;tomato,potato;potato,onion;onion,cucumber
Expected Output
Download Test 4 Input
carrot INF
cucumber INF
onion INF
potato INF
tomato INF
Copyright 2012-2021 by HireVue. All rights reserved. Unauthorized copying, publication, or disclosure prohibited.
 */
public class Test5 {
	/**
	 * Iterate through each line of input.
	 */
	public static void main(String[] args) throws IOException {
		InputStreamReader reader = new InputStreamReader(System.in, StandardCharsets.UTF_8);
		BufferedReader in = new BufferedReader(reader);
		String line;
		String source = "";
		String edges = "";
		int x = 0;
		while ((line = in.readLine()) != null) {
			if (x == 0) {
				source = line;
			} else {
				edges = line;
			}
			x++;
			if(x==2){
				break;
			}
		}
		System.out.println(source);
		System.out.println(edges);
		

		Set<String> nums = getAllkeys(edges);
		Map<String,Set<String>> pairs= getAllMaps(edges);
		for(String num:nums){
			if(num.equals(source)){
				continue;
			}
			System.out.println(num + " " +getDistance(pairs, num, source));
		}
		
		// path("A","A,B;A,C;B,D;C,F;E,G;10,20");
		//path(source, edges);

	}
	 private static void path(String src,String edges){
	        String[] a=edges.split(";");
	        Map<String, List<String>> map= Arrays.stream(a).collect(Collectors.groupingBy(s->s.split(",")[0],
	                Collectors.mapping(e->e.split(",")[1],Collectors.toList())));
	        System.out.println(map);
	            List<String> vertices=map.remove(src);
	            if(null!=vertices){
	                printPath(map,vertices,1);
	            }
	        Stream.concat(map.values().stream().flatMap(Collection::stream),map.keySet().stream()).distinct().forEach(s -> {
	                System.out.println(s+" INF");
	            });
	    }
	    private static void printPath(Map<String,List<String>> map,List<String> vertices,int l){
	        if(null==vertices)
	            return;
	        for (int i = 0; i < vertices.size(); i++) {
	            String v=vertices.get(i);
	            System.out.println(v+" "+l);
	            printPath(map,map.remove(v),l+1);
	        }

	    }
	public static String getDistance(Map<String,Set<String>> pairs, String num, String source){
		int count=0; String key=""; boolean isFound=false; String val="";
		for(Map.Entry<String, Set<String>> maps: pairs.entrySet()){
			if(maps.getValue().contains(num)){
				key= maps.getKey(); count=1;
				break;
				
			}
		}
		while(true){
			if(key.equals(source)){
				val= String.valueOf(count);
				//isFound = true;
				break;
			}
			if(!pairs.containsKey(key)){
				break;
			}else{
				for(Map.Entry<String, Set<String>> maps: pairs.entrySet()){
					if(maps.getValue().contains(key)){
						key= maps.getKey(); count++;
						break;
						
					}
				}
			}
		}
		
		return val.isEmpty()? "INF" : val;
	}

	public static Set<String> getAllkeys(String str) {
		Set<String> set = new HashSet<>();
		String[] edges = str.split(";");
		for (int i = 0; i < edges.length; i++) {
			String[] temp = edges[i].split(",");
			for(int j=0;j<temp.length;j++){
				set.add(temp[j]);
			}
		}
		return set;
	}

	public static Map<String, Set<String>> getAllMaps(String str) {
		Map<String, Set<String>> maps = new HashMap<>();
		String[] edges = str.split(";");
		for (int i = 0; i < edges.length; i++) {
			String[] temp = edges[i].split(",");
			if (temp.length > 1) {
				if (maps.containsKey(temp[0])) {
					Set<String> tempSet = maps.get(temp[0]);
					tempSet.add(temp[1]);
					maps.put(temp[0], tempSet);
				} else {
					Set<String> tempSet = new HashSet<>();
					tempSet.add(temp[1]);
					maps.put(temp[0], tempSet);
				}
				if (maps.containsKey(temp[1])) {
					Set<String> tempSet = maps.get(temp[1]);
					tempSet.add(temp[0]);
					maps.put(temp[1], tempSet);
				} /*else {
					Set<String> tempSet = new HashSet<>();
					tempSet.add(temp[0]);
					maps.put(temp[1], tempSet);
				}*/
			}

		}
		return maps;

	}
}
