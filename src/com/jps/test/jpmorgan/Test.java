package com.jps.test.jpmorgan;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {

	  private static final Pattern pattern = Pattern.compile("[:;][-~]?[)D]");

	  // arguments are passed using the text field below this editor
	  public static void main(String[] args)
	  {
		//  add((byte)1);

	   // List<String> arrow = Arrays.asList(":-) ;~D :) ;D", "sdgsfs :-)");

	   // System.out.println("found: " + countSmileys(arrow));

	  }
	  public static  void add(int x){
		  System.out.println("int add");
	  }
	  public static void add(byte x){
		  System.out.println("byte add");
	  }

	    public static int countSmileys(List<String> arrow) {

	      int count = 0;
	      for (String x : arrow) {
	          Matcher matcher = pattern.matcher(x);

	          while(matcher.find()) {
	              count++;
	          }            
	      }

	      return count;
	    }

	}
