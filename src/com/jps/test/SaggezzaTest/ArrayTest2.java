package com.jps.test.SaggezzaTest;

import java.util.Arrays;
import java.util.List;

//57,34,47,9,12,70,27,28,1978,66,4,14,79,22
// o/p- 34,12,70,28,90,1978,66,4,14,22
public class ArrayTest2 {
public static void main(String[] args) {
	//final java.lang.reflect.Array a= {57,34,47,9,12,70,27,28,1978,66,4,14,79,22};
	Integer[] arr= {57,34,47,9,12,70,27,28,1978,66,4,14,79,22};
	List<Integer> list= Arrays.asList(arr);
	list.stream().filter(i->i%2==0).forEach(System.out::print);
	
}
}
