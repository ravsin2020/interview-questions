package com.jps.test.SaggezzaTest;

public class TP {
public static void main(String[] args) {
	//TP.class.getI
	System.out.println("Value " + testMethod());
}

private static int testMethod() { // c error This method must return a result of type int
	int i=0;
	try {
		i=100/0;
		return i;
	} catch (Exception e) {
		return i=200;
	}
	finally {
		i=300;
	}
	//return null;
}
}
