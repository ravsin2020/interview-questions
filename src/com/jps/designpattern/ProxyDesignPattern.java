package com.jps.designpattern;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Ravikant.singh
 * 
 *         Proxy Design Pattern Proxy means �in place of�, representing� or �in
 *         place of� or �on behalf of� are literal meanings of proxy and that
 *         directly explains Proxy Design Pattern. Proxies are also called
 *         surrogates, handles, and wrappers. They are closely related in
 *         structure, but not purpose, to Adapters and Decorators.
 * 
 *         A real world example can be a cheque or credit card is a proxy for
 *         what is in our bank account. It can be used in place of cash, and
 *         provides a means of accessing that cash when required. And that�s
 *         exactly what the Proxy pattern does � �Controls and manage access to
 *         the object they are protecting�.
 * 
 *         When to use this pattern? Ans: Proxy pattern is used when we need to
 *         create a wrapper to cover the main object�s complexity from the
 *         client.
 * 
 *         Benefits: One of the advantages of Proxy pattern is security. This
 *         pattern avoids duplication of objects which might be huge size and
 *         memory intensive. This in turn increases the performance of the
 *         application. The remote proxy also ensures about security by
 *         installing the local code proxy (stub) in the client machine and then
 *         accessing the server with help of the remote code.
 *
 */
public class ProxyDesignPattern {
	public static void main(String[] args){
		try {
			Internet internet= new ProxyInternet();
			internet.connectTo("accoliteindia.com");
			internet.connectTo("abc.com");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Loading File Example");
		Image realImage= new ProxyImage("Test001.jpg");
		realImage.dispaly();
	}
}

interface Internet {
	public void connectTo(String hostName) throws Exception;
}

class RealInternet implements Internet {
	@Override
	public void connectTo(String hostName) throws Exception {
		System.out.println("Connecting to : " + hostName);
	}
}

class ProxyInternet implements Internet {
	Internet internet = new RealInternet();
	private static List<String> bannedSites;
	static {
		bannedSites = new ArrayList<String>();
		bannedSites.add("abc.com");
		bannedSites.add("def.com");
		bannedSites.add("ijk.com");
		bannedSites.add("lnm.com");
	}

	@Override
	public void connectTo(String hostName) throws Exception {
		if (bannedSites.contains(hostName)) {
			throw new Exception("This site is banded - " + hostName);
		}
		internet.connectTo(hostName);
	}

}

interface Image {
	public void dispaly();
}

class RealImage implements Image {
	private String fileName;

	public RealImage(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public void dispaly() {
		loadFromDesk();
		System.out.println("Displaying Real Image");
	}

	public void loadFromDesk() {
		System.out.println("Loading from disk " + fileName);
	}
}
class ProxyImage implements Image{
	private Image realImage;
	private String fileName;
	public ProxyImage(String fileName){
		this.fileName=fileName;
	}
	@Override
	public void dispaly() {
		if(realImage==null){
			realImage = new RealImage(fileName);
		}
		realImage.dispaly();
	}
	
}