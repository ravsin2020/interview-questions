package com.jps.problemsolving;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Ravi
 *Given an array of integers and a value, 
 *determine if there are any three integers in the array whose sum equals the given value.
 *time complexity -> O(n^2)
 *Space Complexity -> O(N)
 */
public class SumOfThreeIntegersIsEqualToTheGivenValue {
	public static void main(String[] args) {
		int[] input={3,7,1,2,8,4,5}; int sum=20;
		trisum(input, sum);
		System.out.println("**");
		int[] arr = {-25, -10, -7, -3, 2, 4, 8, 10};
		trisum(arr, -8);
		trisum(arr, -25);
		trisum(arr, 0);
		trisum(arr, -42);
		trisum(arr, 42);
		trisum(arr, 22);
		trisum(arr, -7);
		trisum(arr, -3);
		trisum(arr, 2);
		trisum(arr, 4);
		trisum(arr, 8);
		trisum(arr, 1);
	}
	
	public static void trisum(int[] arr, int sum){
		Set<Integer> set= new HashSet<>();
		for (int i = 0; i < arr.length-2; i++) {
			int x = sum - arr[i];
			for(int j = i+1;j<arr.length;j++){
				set.remove(arr[j]);
				if(set.contains(x-arr[j])){
					System.out.println( "For " + sum + " Numbers are : " + arr[i] +" , " 
				+ arr[j] + " , "+ (x - arr[j]));
					return;
				}
				set.add(arr[j]);
			}
		}
		System.out.println("For " + sum +  " Solution not possible");
	}
	
	public static void trisum2(int[] arr, int sum){
		Arrays.sort(arr);
		int start =0; int end=arr.length;
		for (int i = 0; i < arr.length-2; i++) {
			int x = sum - arr[i];
			while(end>start){
				//set.remove(arr[j]);
				//if(set.contains(x-arr[j])){
				//	System.out.println( "For " + sum + " Numbers are : " + arr[i] +" , " 
				//+ arr[j] + " , "+ (x - arr[j]));
				//	return;
				//}
				//set.add(arr[j]);
			}
		}
		System.out.println("For " + sum +  " Solution not possible");
	}
}
