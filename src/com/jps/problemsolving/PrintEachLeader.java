package com.jps.problemsolving;

/**
 * 
 * @author Ravikant.Singh Print each number is leader or not a leader. 12 34 14
 *         35 19 22 25 23 all lead --> 35 25 23
 *
 */
public class PrintEachLeader {
	public static void main(String[] args) {
		int[] input = { 12, 34, 14, 35, 19, 22, 25, 23 };
		printLeader(input);
	}

	public static void printLeader(int[] arr) {
		int maxVal = arr[arr.length - 1];
		System.out.println(arr[arr.length - 1]);
		for (int i = arr.length - 2; i >= 0; i--) {
			if (arr[i] > maxVal) {
				System.out.println(arr[i]);
				maxVal = arr[i];
			}
		}
	}
}
