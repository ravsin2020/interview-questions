package com.jps.problemsolving;

import java.util.Arrays;

/**
 * Given an array of DISTINCT elements
 * 
 * @author Ravikant.singh Input: input arr[] = {4, 3, 7, 8, 6, 2, 1} Output:
 *         arr[] = {3, 7, 4, 8, 2, 6, 1}
 * 
 */
public class ConvertArrayIntoZigZagFashion {
	public static void main(String[] args) {
		int[] arr = {4, 3, 7, 6,8,9,10,11,12 ,2, 1};//{ 4, 3, 7, 6, 2, 1};
		arr = reArrangeArray(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}

	}

public static int[] reArrangeArray2(int[] arr2){
	Arrays.sort(arr2);
	int[] res= new int[arr2.length];
	for (int i = 0; i < res.length/2; i++) {
		
	}
	return arr2;
}

	public static int[] reArrangeArray(int[] arr) {
		if (arr == null || arr.length < 3) {
			return arr;
		}
		boolean flag = true;
		for (int i = 0; i < arr.length - 1; i++) {
			if (flag) {
				if (arr[i] > arr[i + 1]) { // arr[i] expected less
					// swap them
					arr[i + 1] = arr[i] + arr[i + 1];
					arr[i] = arr[i + 1] - arr[i];
					arr[i + 1] = arr[i + 1] - arr[i];
				}
			} else {
				if (arr[i] < arr[i + 1]) { // arr[i] expected more
					// swap them
					arr[i + 1] = arr[i] + arr[i + 1];
					arr[i] = arr[i + 1] - arr[i];
					arr[i + 1] = arr[i + 1] - arr[i];
				}
			}
			flag = !flag;
		}
		return arr;
	}
}
