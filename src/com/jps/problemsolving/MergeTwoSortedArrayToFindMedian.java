package com.jps.problemsolving;
/*
 * Ravikant.Singh
 * Given two sorted arrays, a[] and b[], 
 * the task is to find the median of these sorted arrays, in O(log n + log m) time complexity, 
 * when n is the number of elements in the first array, and m is the number of elements in the second array.
   This is an extension of median of two sorted arrays of equal size problem. Here we handle arrays of unequal size also. 
 */
public class MergeTwoSortedArrayToFindMedian {
	public static void main(String[] args) {
		int ar1[] = {-5, 3, 6, 12, 15};
        int ar2[] = {-12, -10, -6, -3, 4, 10};
        
        int[] arr3 ={100001};
        int[] arr4= {100000};
        
        int[] arr5 ={2,3};
        int[] arr6 ={1};
        System.out.println(findMedian2(arr5, arr6));
	}
	public static double findMedian2(int[] arr1, int[] arr2){
		double val = getMiddleArrIsNull(arr1, arr2);
		if(val!=-1.00){
			return val;
		}
		int[] temp= mergeArray(arr1,arr2);
		
        return getMiddle(temp);
	}
	public static double getMiddleArrIsNull(int[] a,int[] b){
		if(a.length==0 && b.length==0){
			return 0.00;
		}
		if(b.length==0 && a.length==1){
			return (double)a[0];
		}
		if(a.length==0 && b.length==1){
			return (double)b[0];
		}
		if(a.length==1 && b.length==1){
			return (double)(b[0]+a[0])/2;
		}
		if(b.length==0 && a.length>1){
			 return getMiddle(a);
		}
		if(a.length==0 && b.length>1){
			 return getMiddle(b);
		}
		return -1.00;
	}
	private static double getMiddle(int[] temp) {
		if(temp.length%2 != 0){
			return (double)temp[temp.length/2];
		}else{
			return (double)(temp[temp.length/2]+temp[temp.length/2 -1])/2 ;
		}
	}
	private static int[] mergeArray(int[] arr1, int[] arr2) {
		int l1=0, l2=0 ; int i=0;
		int[] temp= new int[arr1.length+arr2.length];
		for(i=0;i<temp.length;i++){
			if(l1==arr1.length || l2==arr2.length){
				break;
			}
			if(arr1[l1]<=arr2[l2]){
				temp[i]=arr1[l1];
				l1++;
			}else{
				temp[i]=arr2[l2];
				l2++;
			}
		}
		if(arr1.length>l1){
			while(i<temp.length){
				temp[i]=arr1[l1];
				l1++; i++;
			}
		}
		if(arr2.length>l2){
			while(i<temp.length){
				temp[i]=arr2[l2];
				l2++;i++;
			}
		}
		return temp;
	}
	public static double findMedian(int[] arr1, int[] arr2){
		double val=getMiddleArrIsNull(arr1, arr2);
		if(val!=-1.00){
			return val;
		}
		
		return -1;
	}
	public static double getMiddle(int a,int b){
		return (double) ((a+b)/2);
	}
}
