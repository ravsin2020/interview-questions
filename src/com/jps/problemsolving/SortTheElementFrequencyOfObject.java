package com.jps.problemsolving;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class SortTheElementFrequencyOfObject<T> {
	public static void main(String[] args) {
		SortTheElementFrequencyOfObject processor = new SortTheElementFrequencyOfObject<>();
		Integer[] arr = { 2, 2, 3, 3, 4, 5, 6, 7, 8, 8, 8, 9, 9, 1, 1, 3, 4 };
		List<Integer> list = Arrays.asList(arr);
		System.out.println(list.size());
		processor.doSortAndPrint(list);
		System.out.println("**********");
		Employee emp1 = new Employee(10, "Ravi");
		Employee emp2 = new Employee(10, "Ravi");
		Employee emp3 = new Employee(20, "Shyam");
		Employee emp4 = new Employee(10, "Shiva");
		Employee emp5 = new Employee(10, "Shiva");
		Employee emp6 = new Employee(10, "Raj");
		Employee emp7 = new Employee(10, "Raj");
		Employee emp8 = new Employee(10, "Raj");
		List<Employee> empList = new ArrayList<>();
		empList.add(emp1);
		empList.add(emp2);
		empList.add(emp3);
		empList.add(emp4);
		empList.add(emp5);
		empList.add(emp6);
		empList.add(emp7);
		empList.add(emp8);
		processor.doSortAndPrint(empList);

	}

	public void doSortAndPrint(List<T> list) {
		Map<T, Integer> map = new HashMap<>();
		for (T t : list) {
			if (map.containsKey(t)) {
				map.put(t, map.get(t) + 1);
			} else {
				map.put(t, 1);
			}
		}
		Set<Map.Entry<T, Integer>> empSet = map.entrySet();
		List<Map.Entry<T, Integer>> empList = new ArrayList(empSet);
		Collections.sort(empList, new SortTheObjectInDec());
		for (Map.Entry<T, Integer> object : empList) {
			System.out.println(object.getKey() + " : " + object.getValue());
		}
	}

	class SortTheObjectInDec implements Comparator<Map.Entry<T, Integer>> {
		@Override
		public int compare(Entry<T, Integer> arg0, Entry<T, Integer> arg1) {
			return (arg1.getValue().compareTo(arg0.getValue()));
		}
	}

	static class Employee {
		private int id;
		private String name;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Employee(int id, String name) {
			super();
			this.id = id;
			this.name = name;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + id;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Employee other = (Employee) obj;
			if (id != other.id)
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "Employee [id=" + id + ", name=" + name + "]";
		}
	}
}
