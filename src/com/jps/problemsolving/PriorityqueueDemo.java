package com.jps.problemsolving;

import java.util.PriorityQueue;

public class PriorityqueueDemo {
	public static void main(String[] args) {
		PriorityQueue<Integer> pq= new PriorityQueue<>();
		pq.add(10);
		pq.add(49);
		pq.add(4);
		pq.add(9);
		pq.add(50);
		for (Integer integer : pq) {
			System.out.println(integer);
		}
	}
	
}
