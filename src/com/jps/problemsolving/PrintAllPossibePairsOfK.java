package com.jps.problemsolving;

/*
 * print all possible pairs 
 * array is sorted array
 */
public class PrintAllPossibePairsOfK {
	public static void main(String[] args) {
		//Sorted array
		int[] arr = { 1, 3, 4, 6, 7 };
		printAllPossibePairs2(arr, 7);
		// System.out.println(doSearch(arr, 6, 0, arr.length));
		System.out.println("**************");
		printAllPossibePairs1(arr, 7);
	}
	// if array is sorted
	public static void printAllPossibePairs1(int[] arr, int k) {
		int start=0,end=arr.length-1;
		while (end>start) {
			if(arr[start]+arr[end]==k){
				System.out.println("pair - " + arr[start] + " " + arr[end]);
				start++;end--;
			}else if(k-arr[start]<arr[end]){
				end--;
			}else{
				start--;
			}
		}
	}

	public static void printAllPossibePairs2(int[] arr, int k) {
		for (int i = 0; i < arr.length; i++) {
			int val = doSearch(arr, arr[i], k - arr[i], 0, arr.length);
			if(val !=-1){
				//System.out.println(arr[i] +  "," + arr[val]);
			}
		}
	}

	public static int doSearch(int[] arr,int val, int k, int start, int end) {
		if (end >= start) {
			int mid = start + (end - start) / 2;
			if (arr[mid] == k) {
				System.out.println(val +  "," + arr[mid]);
				return mid;
			}
			if (arr[mid] > k) {
				doSearch(arr, val, k, start, mid- 1);
			} else {
				doSearch(arr, val, k, mid + 1, end);
			}
		}
		return -1;
	}

}
