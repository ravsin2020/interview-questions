package com.jps.problemsolving;

import java.util.HashMap;
import java.util.Map;
/**
 * 
 * @author Ravikant.Singh
 * 
 * Given an array of integers nums and an integer target, 
 * return indices of the two numbers such that they add up to target.
 *
 */
class GivenIntegersNumsAndAnIntegerTarget {
	public static void main(String[] args) {
		int[] arr = {2,7,11,15};
		int target = 9;
		int[] res =twoSum(arr, target) ;
		System.out.println(res[0] + " , " + res[1]);
		//Map<K, V>
	}
	 public static int[] twoSum(int[] nums, int target) {
	        Map<Integer,Integer> map= new HashMap<>();
	        for(int i=0; i<nums.length;i++){
	            if(map.containsKey(target - nums[i])){
	            	return new int[] { map.get(target - nums[i]), i };
	            }else{
	                map.put(nums[i],i);
	            }
	        }
	        return null;
	    }
}
