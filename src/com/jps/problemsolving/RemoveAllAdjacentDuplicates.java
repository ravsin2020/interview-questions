package com.jps.problemsolving;

public class RemoveAllAdjacentDuplicates {
	public static void main(String[] args) {
		//System.out.println("azxxzy : " + removeDuplicates("azxxzy"));
		//System.out.println("keexxllx : " + removeDuplicates("keexxllx"));
		//System.out.println("abbaca : " + removeDuplicates("abbaca"));
		//System.out.println("cabbac : " + removeDuplicates("cabbac"));
		//System.out.println("acaaabbbacdddd : " + removeDuplicates("acaaabbbacdddd")); //
		//System.out.println("geeksforgeek : " + removeDuplicates("geeksforgeek"));
		//System.out.println("abccbccba: " + removeDuplicates("abccbccba"));
		// abbba // null
		System.out.println("ississi : " + removeDuplicates("ississi"));
	}

	public static String removeDuplicates(String str) {
		if (str == null || str.length() <= 1) {
			return str;
		}
		int end = str.length();
		boolean found = true;
		//while (found) {
			int i = 0;
			char lastRemoved = ' ';
			found = false;
			while (i < end) {
				if (i < end - 1 && str.charAt(i) == str.charAt(i + 1)) {
					lastRemoved = str.charAt(i);
					str = str.substring(0, i) + str.substring(i + 2, end);
					end = str.length();
					found = true;
					//i = (i == 0 ? 0 : i - 1);
				} else if (str.charAt(i) == lastRemoved) {
					str = str.substring(0, i) + str.substring(i + 1, end);
					end = str.length();
					found = true;
					i = (i == 0 ? 0 : i - 1);
				}else {
					i++;
					lastRemoved = ' ';
				}
			}
		//}
		return str;
	}
}
