package com.jps.problemsolving;

import java.util.Stack;

import org.apache.poi.util.StringUtil;

/**
 * 
 * @author Ravikant.Singh
 *
Example->
(Abc)) —> (Abc)

((Abc) —> (Abc)

(Abc)((abc))) —> (Abc)((abc))
(Abc)(((abc)) —> (Abc)((abc))

((Asd)(abc))) —> ((Asd)(abc))

 */
public class BalanceStringParenthesis {
public static void main(String[] args) {
	System.out.println("Input - (((Abc : o/p - " + getBalancedString(")(((Abc)"));
	
	System.out.println("Input - ((Asd)(abc))) : o/p - " + getBalancedString("((Asd)(abc)))"));
}

public static String getBalancedString(String str){
	Stack<Integer> s= new Stack<>();
	for (int i = 0; i < str.length(); i++) {
		if(str.charAt(i)=='('){
			s.push(i);
		}else if(str.charAt(i)==')' && s.isEmpty()){
			s.push(i);
		}else if(str.charAt(i)==')' && !s.isEmpty()){
				s.pop();
		}
	}
	String result="";
	int end=str.length();
	while(!s.isEmpty()){
		result = str.substring(s.peek()+1, end) + result;
		end=s.pop();
	}
	if(end>0){
		result = str.substring(0, end) + result;
	}
	return result;
	
}
}
