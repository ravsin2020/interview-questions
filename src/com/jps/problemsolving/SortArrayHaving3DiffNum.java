package com.jps.problemsolving;

public class SortArrayHaving3DiffNum {
	public static void main(String[] args) {
		int[] arr = { 5, 9, 7, 7, 5, 9, 7, 5, 9, 7, 7, 5, 9, 7, 9, 5, 9, 7 };
		arr = sortArray(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
	}

	public static int[] sortArray(int[] arr) {
		if (arr == null || arr.length <= 1) {
			return arr;
		}
		int start = 0; int end = arr.length - 1; int mid = 0;
		while (mid <= end) {
			switch (arr[mid]) {
			case 5:
				int temp = arr[mid];
				arr[mid] = arr[end];
				arr[end] = temp;
				end--;
				break;
			case 7:
				mid++;
				break;
			case 9:
				temp = arr[start];
				arr[start] = arr[mid];
				arr[mid] = temp;
				start++; mid++;
				break;
			}
		}

		return arr;
	}
}
