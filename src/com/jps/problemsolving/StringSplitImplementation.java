package com.jps.problemsolving;

import java.util.ArrayList;
import java.util.List;

public class StringSplitImplementation {
	public static void main(String[] args) {
		List<String> strList = StringImpl.spilt("I#am#Ravi", '#');
		strList.stream().forEach(e -> {
			System.out.println(e);
		});
	}

	private static class StringImpl {
		public static List<String> spilt(String str, char del) {
			if (str == null || str.length() == 0) {
				return null;
			}
			StringBuffer word = new StringBuffer();
			List<String> list = new ArrayList<>();
			for (int i = 0; i < str.length(); i++) {
				if (str.charAt(i) != del) {
					word.append(str.charAt(i));
				} else {
					list.add(word.toString());
					word = new StringBuffer();
				}
			}
			if (word.length() > 0) {
				list.add(word.toString());
			}
			return list;
		}
	}
}
