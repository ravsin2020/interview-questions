package com.jps.problemsolving;

public class TokenGeneration {
public static void main(String[] args) {
	System.out.println("70200 : " );
	 encode(70200);
	 System.out.println("70201 : " );
	 encode(70201);
	 System.out.println("18,27,800 : ");
	 encode(1827800);
	 System.out.println("18,27,801 : ");
	 encode(1827801);

}
private static void encode(int num){
    int r=num%100;
    num/=100;
    if(r!=0){
        num+=1;
    }
    StringBuffer sb=new StringBuffer();
    while (num>0){
        r=num%26;
        if(r==0){
            sb.append('Z');
            num=(num/26)-1;
        }
        else {
            sb.append((char)('A'+r-1));
            num/=26;
        }
    }
    System.out.println(sb.reverse());
    }
}
