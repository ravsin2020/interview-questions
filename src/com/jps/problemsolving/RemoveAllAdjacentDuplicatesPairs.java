package com.jps.problemsolving;

public class RemoveAllAdjacentDuplicatesPairs {
	public static void main(String[] args) {
		System.out.println("azxxzy : " + removeDuplicates("azxxzy"));
		System.out.println("keexxllx : " + removeDuplicates("keexxllx"));
		System.out.println("abbaca : " + removeDuplicates("abbaca"));
		System.out.println("cabbac : " + removeDuplicates("cabbac"));
		System.out.println("acaaabbbacdddd : " + removeDuplicates("acaaabbbacdddd"));
		System.out.println("abccbccba: " + removeDuplicates("abccbccba"));
	}

	public static String removeDuplicates(String str) {
		if (str == null || str.length() <= 1) {
			return str;
		}
		int end = str.length();
		int i = 0;
		while (i < end-1) {
			if (str.charAt(i) == str.charAt(i + 1)) {
				str = str.substring(0, i) + str.substring(i + 2, end);
				i = (i == 0 ? 0 : i - 1);
				end = str.length();
			} else {
				i++;
			}
		}
		return str;
	}
}
