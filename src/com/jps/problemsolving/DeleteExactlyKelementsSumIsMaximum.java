package com.jps.problemsolving;
/**
 * 
 * @author Ravikant.Singh
 * 
 Given an array, delete exactly k elements from the array so that the sum of remaining elements is maximum. 
Deletion can only be done from the start or the end of the array.

[1,2,6,4,3,2,1,4]

Example,

K=1

[1,2,6,4,3,2,1,4] ->[2,6,4,3,2,1,4]
        
K=2

[1,2,6,4,3,2,1,4] -> [6,4,3,2,1,4]

K=3

[1,2,6,4,3,2,1,4] -> [6,4,3,2,1]

K=4 
[1,2,6,4,3,2,1,4] -> [6,4,3,2]
 *
 */
public class DeleteExactlyKelementsSumIsMaximum {
	public static void main(String[] args) {
		int[] input= {1,2,6,4,3,2,1,4};
		int[] sol = findSubArray(input, 4);
		for (int i = 0; i < sol.length; i++) {
			System.out.println(sol[i]);
		}
	}

	public static int[] findSubArray(int[] arr, int k) {
		if (k < arr.length - 1) {
			int start = 0;
			int end = arr.length - 1;
			for (int i = 0; i < k; i++) {
				if(arr[start]<arr[end]){
					start++;
				}else{
					end--;
				}
		}
			return getSubArray(arr, start , end);
		}
		return arr;
	}

	private static int[] getSubArray(int[] arr, int start, int end) {
		int[] sol = new int[(end-start)+1]; int count=0;
		for (int i = start; i < end+1; i++) {
			sol[count] = arr[i];
			count++;
		}
		return sol;
	}

}
