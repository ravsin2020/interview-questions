package com.jps.java8;

public class DefaultAndStaticTest {
	public static void main(String[] args) {

	}
}

@FunctionalInterface
interface Interface1 {
	void method1(String str);

	default void log(String str) {
		System.out.println("I1 logging::" + str);
	}

	static void print(String str) {
		System.out.println("Printing " + str);
	}
}

interface Interface2 {
	void method2();

	default void log(String str) {
		System.out.println("I2 logging::" + str);
	}
}

class MyClass implements Interface1, Interface2 {

	@Override
	public void method2() {
		System.out.println("method2()");

	}

	@Override
	public void method1(String str) {
		System.out.println("method1(String str)");

	}

	@Override
	public void log(String str) {
		Interface1.super.log(str);
	}

}

interface Vehicle {
	default void print() {
		System.out.println("Vehicle");
	}
}

interface FourWheeler {
	default void print() {
		System.out.println("FourWheeler");
	}
}

class Car implements Vehicle, FourWheeler {
	@Override
	public void print() {
		Vehicle.super.print();
	}
}
