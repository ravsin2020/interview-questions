package com.jps.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SumOfSalEmplyeeAgeMoreThen30 {
	public static void main(String[] args) {
		List<Employee> emps= new ArrayList<>();
		Employee emp1= new Employee("Ravi", 20, 3000.00);
		Employee emp2= new Employee("Shyam", 31, 4020.90);
		Employee emp3= new Employee("Shashi", 40, 5000.00);
		emps.add(emp1);
		emps.add(emp2);
		emps.add(emp3);
		Double sal= emps.stream().filter(e->e.getAge()>30).mapToDouble(i->i.getSalary()).sum();
		System.out.println("Total Salary : "+ sal);
		
		
	}
}
