package com.jps.java8;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class GetAllEmplyeeNameStartsWithSAndSortWithName {
	public static void main(String[] args) {
		List<Employee> emps= new ArrayList<>();
		Employee emp1= new Employee("Ravi", 20, 5000.00);
		Employee emp2= new Employee("Shyam", 30, 5000.00);
		Employee emp3= new Employee("Shashi", 40, 5000.00);
		Employee emp4= new Employee("Suman", 50, 5000.00);
		emps.add(emp1);
		emps.add(emp2);
		emps.add(emp3);
		emps.add(emp4);
		List<Employee> finalList= emps.stream().filter(e->e.getName().startsWith("S")).collect(Collectors.toList());
		finalList.forEach(System.out::println);
		System.out.println("**");
		List<Employee> finalList2= emps.stream().sorted(Comparator.comparing(Employee::getName))
				.collect(Collectors.toList());
		finalList2.forEach(System.out::println);
		
		
	}
	class EmployeeNameSorting implements Comparator<Employee>{

		@Override
		public int compare(Employee o1, Employee o2) {
			
			return o1.getName().compareTo(o2.getName());
		}
		
	}
}
