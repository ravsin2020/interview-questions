package com.jps.java8;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DateTimeChanges {
public static void main(String[] args) {
	LocalDate localDate = LocalDate.now();
	System.out.println(localDate);
	LocalTime localTime = LocalTime.of(12, 20);
	System.out.println(localTime);
	LocalDateTime localDateTime = LocalDateTime.now(); 
	System.out.println(localDateTime);
	OffsetDateTime offsetDateTime = OffsetDateTime.now();
	System.out.println(offsetDateTime);
	ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
	System.out.println(zonedDateTime);
	
	Instant instant = Instant.now();
	System.out.println(instant);
	Instant instant1 = instant.plus(Duration.ofMillis(5000));
	System.out.println(instant1);
	Instant instant2 = instant.minus(Duration.ofMillis(5000));
	System.out.println(instant2);
	Instant instant3 = instant.minusSeconds(10);
	System.out.println(instant3);
}
}
