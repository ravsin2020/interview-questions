package com.jps.java8;

import java.util.ArrayList;
import java.util.List;

public class StreamParallelSeqDemo {
public static void main(String[] args) {
	List<Integer> list= new ArrayList<>();
	for(int i=0;i<100;i++){
		list.add(i);
	}
	System.out.println("Parallel Stream");
	list.parallelStream().filter(e->e>90).forEach(System.out::println);
	System.out.println("Serial Stream");
	list.stream().filter(p->p>90).forEach(System.out::println);
}
}
