package com.jps.java8;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SortTheFreqInDescAndAscOrder {
	public static void main(String[] args) {
		String[] arr={"aa","aa","bb","ccc","11","11","11"};
		System.out.println("get Freq in Asc : " + getFreqInAscOredr(Arrays.asList(arr)));
		System.out.println("get Freq in Desc : " + getFreqInDescOredr(Arrays.asList(arr)));
		System.out.println("*********");
		Arrays.asList(arr).stream().sorted().forEach(System.out::println);
		Arrays.asList(arr).stream().sorted(Comparator.reverseOrder()).forEach(System.out::println);
	}

	public static Map<String, Long> getFreqInAscOredr(List<String> list) {
		Map<String, Long> map = list.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		return map.entrySet().stream().sorted(Map.Entry.comparingByValue())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, 
						(e1, e2) -> e1, LinkedHashMap::new));
	}
	public static Map<String, Long> getFreqInDescOredr(List<String> list) {
		Map<String, Long> map = list.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		return map;
		//return map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
		//		.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}
}
