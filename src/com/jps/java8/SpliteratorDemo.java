package com.jps.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class SpliteratorDemo {
public static void main(String[] args) {
	List<Integer> ids= new ArrayList<>();
	for (int i = 10; i < 100; i++) {
		ids.add(i);
	}
	Spliterator<Integer> idsSpliterator= ids.spliterator();
	idsSpliterator.forEachRemaining(System.out::println);
	
	System.out.println("****sum**");
	Integer sum= ids.stream().collect(Collectors.summingInt(Integer::intValue));
	System.out.println(sum);
}
}
