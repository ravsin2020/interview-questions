package com.jps.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class GetNthHighest {
	public static void main(String[] args) {
		Integer[] arr= {9,3,7,7,0,8,9,6,4,2,1,0,99,88};
		List<Integer> list= Arrays.asList(arr); int k=1;
		Integer val = list.stream().sorted(Collections.reverseOrder()).skip(1).findFirst().get();
		System.out.println(val);
		System.out.println("**************");
		List<Employee2> employeeList= getEmployeeList();
		int nTh= 1;
		Optional<Employee2> emp = employeeList.stream()
		        .sorted(Comparator.comparingDouble(Employee2::getSalary).reversed()).skip(nTh).findFirst();

		System.out.println(nTh+1 + " Highest Salary : " + emp.get());
		
		//************sort this map with value in ascending order************
		//final Map<String, Integer> sortedByCount = wordCounts.entrySet().stream().sorted(Map.Entry.comparingByValue())
		//                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		
	}
	
	private static List<Employee2> getEmployeeList() {
		List<Employee2> list= new ArrayList<>();
		list.add(new Employee2(100,"Ravi", 10, "M", "IT", 2000, 1000));
		list.add(new Employee2(102,"Ravi raj", 11, "M", "IT", 2000, 1080));
		list.add(new Employee2(101,"Ravi Teja", 14, "M", "IT", 2000, 1900));
		return list;
	}

	
}
class Employee2{
	private int id;
     
    private String name;
     
    private int age;
     
    private String gender;
     
    private String department;
     
    private int yearOfJoining;
     
    private double salary;

	public int getId() {
		return id;
	}
	

	public Employee2(int id, String name, int age, String gender, String department, int yearOfJoining,
			double salary) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.department = department;
		this.yearOfJoining = yearOfJoining;
		this.salary = salary;
	}


	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public int getYearOfJoining() {
		return yearOfJoining;
	}

	public void setYearOfJoining(int yearOfJoining) {
		this.yearOfJoining = yearOfJoining;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}


	@Override
	public String toString() {
		return "Employee2 [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", department="
				+ department + ", yearOfJoining=" + yearOfJoining + ", salary=" + salary + "]";
	}
	
    
}