package com.jps.java8;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class CustomAnnotationDemo {

}
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface RoleValidation{
	
}

