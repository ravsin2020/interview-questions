package com.jps.scenario;

import java.util.Map;

public class Person {
private String name;
private String email;
private Map<String,String> mapOfFields;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public Map<String, String> getMapOfFields() {
	return mapOfFields;
}
public void setMapOfFields(Map<String, String> mapOfFields) {
	this.mapOfFields = mapOfFields;
}
}
