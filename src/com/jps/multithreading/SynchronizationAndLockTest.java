package com.jps.multithreading;

import java.util.concurrent.atomic.AtomicInteger;

public class SynchronizationAndLockTest {
public static void main(String[] args){
	Processor11 processor= new Processor11();
	
	Thread th1= new Thread(new Runnable(){
		public void run(){
			processor.incrementCount2();
		}
	});
	
	Thread th2= new Thread(new Runnable(){
		public void run(){
			processor.incrementCount2();
		}
	});
	th1.start();
	th2.start();
}
}

class Processor11{
	//int bc=0;
	AtomicInteger bc = new AtomicInteger() ;
	public int incrementCount(){
		int ab=0;
		System.out.println(Thread.currentThread().getName() +" : " + ab );
		for(int i=0; i<10000 ;i++ ){
			ab++;
		}
		System.out.println(Thread.currentThread().getName() +" : " + ab );
		return ab;
	}
	public void incrementCount2(){
		
		System.out.println(Thread.currentThread().getName() +" : " + bc );
		for(int i=0; i<10000 ;i++ ){
			bc.incrementAndGet();
			//bc++;
		}
		System.out.println(Thread.currentThread().getName() +" : " + bc );
		return ;
	}
}
