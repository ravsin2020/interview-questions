package com.jps.multithreading;

public class ThreadCreationByImplementingRunnable {
	public static void main(String[] args) {
		Thread th= new Thread(new Hi());
		Thread th2= new Thread(new Hello());
		th.start();
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		th2.start();
	}
}

class Hi implements Runnable{
	
	public void run(){
		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Hi Thread is running : " + i+ Thread.currentThread());
		}
	}
}
class Hello implements Runnable{
	
	public void run(){
		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Hello Thread is running : " + i+ Thread.currentThread());
		}
	}
}
