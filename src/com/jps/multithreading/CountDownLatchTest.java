package com.jps.multithreading;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
public class CountDownLatchTest {
	public static void main(String[] args) {
		CountDownLatch latch = new CountDownLatch(3);
		ExecutorService service = Executors.newFixedThreadPool(3);
		for (int i = 0; i < 3; i++) {
			service.submit(new Processor2(latch));
		}
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("All Process completed");
	}
}

class Processor2 implements Runnable {
	CountDownLatch countdownlatch;

	public Processor2(CountDownLatch countdownlatch) {
		this.countdownlatch = countdownlatch;
	}
	@Override
	public void run() {
		System.out.println("Processor Started  : " + countdownlatch);
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {e.printStackTrace();}
		countdownlatch.countDown();
	}

}
