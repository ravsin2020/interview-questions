package com.jps.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TreadPoolTest {
public static void main(String[] args){
	//ExecutorService service = Executors.newFixedThreadPool(2);
	Thread th1= new Thread(new Runnable(){
		@Override
		public void run() {
			System.out.println(Thread.currentThread().getName() +" + Started");
			for(int i=0;i<100;i++){
				System.out.println(Thread.currentThread().getName() + " : " + i);
			}
			System.out.println(Thread.currentThread().getName() + " : End ");
		}
		
	});
	Thread th2= new Thread(new Runnable(){
		@Override
		public void run() {
			try {
				System.out.println(Thread.currentThread().getName() + " : Started ");
				//th1.sleep(10000);//or// Thread.sleep(10000);
				th1.join();
				for(int i=0;i<100;i++){
					System.out.println(Thread.currentThread().getName() + " : " + i);
				}
				System.out.println(Thread.currentThread().getName() + " : End ");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	});
	
	th1.start();
	th2.start();
}
}
