package com.jps.multithreading;

public class SynchronizationTest {
	public static void main(String args[]) {
		Processor10 processor = new Processor10();
		Processor10 processor2 = new Processor10();
		Thread th0 = new Thread(new Runnable() {
			public void run() {
				processor.m0();
			}
		});
		Thread th1 = new Thread(new Runnable() {
			public void run() {
				processor2.m1();
			}
		});
		Thread th2 = new Thread(new Runnable() {
			public void run() {
				processor.m2();
				}
		});
		Thread th3 = new Thread(new Runnable() {
			public void run() {
				processor.m3();
			}
		});
		Thread th4 = new Thread(new Runnable() {
			public void run() {
				processor.m4();
			}
		});
		//th0.start();
		//th1.start();
		th2.start();
		th3.start();
		th4.start();

	}
}

class Processor10 {
	public static synchronized void m0() {
		System.out.println(" static synchronized void m0() start : " + Thread.currentThread().getName());
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(" static synchronized void m0() End : "+ Thread.currentThread().getName());
	}
	public static synchronized void m1() {
		System.out.println(" static synchronized void m1() start : " + Thread.currentThread().getName());
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(" static synchronized void m1() End : " + Thread.currentThread().getName());
	}

	public synchronized void m2() {
		System.out.println("synchronized void m2() start : " + Thread.currentThread().getName());
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("synchronized void m2() End : " + Thread.currentThread().getName());
	}

	public synchronized void  m3() {
		System.out.println(" synchronized void m3() start : " + Thread.currentThread().getName());
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(" synchronized void m3() End  : " + Thread.currentThread().getName());
	}

	public void m4() {
		System.out.println(" void m4() start");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(" void m4() End");
	}
}
