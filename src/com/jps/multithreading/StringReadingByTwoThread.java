package com.jps.multithreading;

public class StringReadingByTwoThread {
public static void main(String[] args) {
	StringProcessor processor= new StringProcessor("Ravikant");
	Thread th1= new Thread(new Runnable(){
		@Override
		public void run(){
			try {
				processor.readOddChar();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	});
	Thread th2= new Thread(new Runnable(){
		@Override
		public void run(){
			try {
				processor.readEvenChar();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	});
	th1.setName("Thread 1");
	th2.setName("Thread 2");
	th1.start();
	th2.start();
}
}

class StringProcessor{
	private String str;
	private static volatile int count=0;
	Object obj= new Object();
	StringProcessor(String str){
		this.str=str;
	}
	
	public void readOddChar() throws InterruptedException{
		while(str.length()-1>count){
			synchronized(obj){
				if(count%2!=0){
					obj.wait();
				}
				System.out.println(Thread.currentThread()+" "+str.charAt(count));
				count+=1;
				obj.notify();
			}
		}
	}
	public void readEvenChar() throws InterruptedException{
		while(str.length()-1>count){
			synchronized(obj){
				if(count%2==0){
					obj.wait();
				}
				System.out.println(Thread.currentThread()+" "+str.charAt(count));
				count+=1;
				obj.notify();
			}
		}
	}
}
