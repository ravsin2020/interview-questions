package com.jps.multithreading;

public class DeadLockProgram {
	public static void main(String[] args) {
		String str1 = "Hai";
		String str2 = "Hello";

		Thread th1 = new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized (str1) {
					System.out.println("Inside Ist Thread , 1 st synchronized block");
					synchronized (str2) {
						System.out.println("Inside Ist Thread , 2nd synchronized block");
					}
				}
			}
		});
		
		Thread th2 = new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized (str2) {
					System.out.println("Inside 2nd Thread , 2nd synchronized block");
					synchronized (str1) {
						System.out.println("Inside 2nd Thread , 2nd synchronized block");
					}
				}
			}
		});
		
		th1.start();
		th2.start();

	}
}
