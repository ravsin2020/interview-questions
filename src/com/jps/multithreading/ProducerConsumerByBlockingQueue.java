package com.jps.multithreading;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ProducerConsumerByBlockingQueue {
	//BlockingQueue is a interface and is thread safe
	static BlockingQueue<Integer> queue= new ArrayBlockingQueue<Integer>(1);
	public static void main(String[] args) {
		ProducerConsumerByBlockingQueue obj= new ProducerConsumerByBlockingQueue();
		Thread producerThread = new Thread(()->{
			try {
				obj.producer();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		
		Thread consumerThread = new Thread(()->{
			try {
				obj.consumer();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		
		producerThread.start();
		consumerThread.start();
	}
	public void producer() throws InterruptedException{
		Random random = new Random();
		while(queue.size()!=10){
			int addedVal= random.nextInt(10);
			queue.put(addedVal);
			System.out.println("Producer produces : " + addedVal);
		}
	}
	public void consumer() throws InterruptedException{
		while(true){
			Thread.sleep(100);
			//if(random.nextInt(10) == 0){
				int takenvalue = (int) queue.take();
				System.out.println("Consumer consumes : " + takenvalue);
			//}
		}
	}
}
