package com.jps.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadPoolExaple1 {
	public static void main(String[] args) {
		ExecutorService service = Executors.newFixedThreadPool(5);
		for (int i = 0; i < 5; i++) {
			service.submit(new Processor4(i));
		}
		service.shutdown();
		System.out.println("All Task started");
		try {
			service.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("All Task DONE");
		
	}
}

class Processor4 implements Runnable {
	int processId;

	public Processor4(int processId) {
		super();
		this.processId = processId;
	}

	@Override
	public void run() {
			System.out.println("Process started : " + processId +" & " + Thread.currentThread());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException exp) {
			}
			System.out.println("Process Ends : " + processId +" & " + Thread.currentThread());
	}

}