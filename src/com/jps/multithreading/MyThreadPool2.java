package com.jps.multithreading;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MyThreadPool2 {
public static void main(String[] args) {
	ThreadPool3 pool= new ThreadPool3(5);
	for (int i = 0; i < 10; i++) {pool.submit(new Task3());}
	pool.shutdown();
}
}

class ThreadPool3 {
	private final int n;  private final WorkerThread[] threads;  private final BlockingQueue queue;
	public ThreadPool3(int n) {
		this.n = n; threads = new WorkerThread[n]; queue = new LinkedBlockingQueue();
		for (int i = 0; i < n; i++) {
			threads[i] = new WorkerThread();
			threads[i].start(); 
			}}
	public void submit(Runnable task) {
		synchronized (queue) {
			queue.add(task);
			queue.notify();
		}
	}
	public void shutdown(){
		for (int i = 0; i < n; i++) {
			threads[i].interrupt();
		}
	}

	class WorkerThread extends Thread {
		public void run() {
			Runnable task;
			while (true) {
				synchronized (queue) {
					while (queue.isEmpty()) {
						try {
							queue.wait();
						} catch (InterruptedException e) {
							//System.out.println("An error occurred while queue is waiting: " + e.getMessage());
							Thread.currentThread().interrupt();
							return;
						}
					}
					task = (Runnable) queue.poll();
				}
				// If we don't catch RuntimeException,
				// the pool could leak threads
				try {
					task.run();
				} catch (RuntimeException e) {
					System.out.println("Thread pool is interrupted due to an issue: " + e.getMessage());
				}
			}
		}

	}
}

class Task3 implements Runnable{
	
	@Override
	public void run() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Task is running");
	}
	
}
