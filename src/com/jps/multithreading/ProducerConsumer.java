package com.jps.multithreading;

public class ProducerConsumer {
public static void main(String[] args) {
	Order order = new Order();
 new Producer(order);
 new Consumer(order);
}
}

class Order{
	
	private volatile int orderCount;
	private volatile boolean isAvailable = false;

	public synchronized void setOrderCount(int orderCount) {
		if(isAvailable){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.orderCount = orderCount;
		isAvailable = true;
		notify();
		
	}

	public synchronized int getOrderCount() {
		if(!isAvailable){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		isAvailable = false;
		notify();
		return orderCount;
	}

	
	
}
class Producer implements Runnable{
	Order count;
	public Producer(Order count){
		this.count = count;
		Thread th1 = new Thread(this, "Producer");
		th1.start();
	}
	@Override
	public void run() {
		int i= 0;
		while(true){
			System.out.println("Producer Produces OrderId : " + i);
			synchronized(count){
				count.setOrderCount(i++);
			}
			try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
		}
		
	}

}
class Consumer implements Runnable{
	Order order;
	public Consumer(Order order){
		this.order = order;
		new Thread(this,"Consumer").start();
	}
	@Override
	public void run() {
		while(true){
			System.out.println("Consumer Consumes OrderId : " + order.getOrderCount());
			try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
		}
	}
	
}