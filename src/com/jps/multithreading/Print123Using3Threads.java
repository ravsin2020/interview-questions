package com.jps.multithreading;

import java.util.concurrent.locks.ReentrantLock;

public class Print123Using3Threads {
public static void main(String[] args) {

	Processor7 processor= new Processor7();
	Thread th1= new Thread(new Runnable(){
		public void run(){
			try {
				processor.read1();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	});
	Thread th2= new Thread(new Runnable(){
		public void run(){
			try {
				processor.read1();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	});
	Thread th3= new Thread(new Runnable(){
		public void run(){
			try {
				processor.read1();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	});
	th1.start();
	th2.start();
	th3.start();

}
}
class Processor7{
	private static volatile int count=1;
	ReentrantLock obj= new ReentrantLock();
	Processor7(){
	}
	public void read1() throws InterruptedException{
		while(count<=3){
			synchronized(obj){
				if("Thread-2".equals(Thread.currentThread().getName()) && count ==3){
					System.out.println(Thread.currentThread().getName()+" "+count);
					count=1;
					obj.tryLock();
					obj.notifyAll();
				}else if( "Thread-1".equals(Thread.currentThread().getName()) && count==2){
					System.out.println(Thread.currentThread().getName()+" "+count);
					count+=1;
					obj.tryLock();
					obj.notifyAll();
				}else if( "Thread-0".equals(Thread.currentThread().getName()) && count==1){
					System.out.println(Thread.currentThread().getName()+" "+count);
					count+=1;
					obj.tryLock();
					obj.notifyAll();
				}
			}
		}
	}

}
