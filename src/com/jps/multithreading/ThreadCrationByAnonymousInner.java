package com.jps.multithreading;

public class ThreadCrationByAnonymousInner {
	public static void main(String[] args) {
		Thread th1 = new Thread(new Runnable() {
			public void run() {
				for (int i = 0; i < 5; i++) {
					try {
						Thread.sleep(1000);
						System.out.println("Thread 1 is running : " + i + " " + Thread.currentThread());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		Thread th2 = new Thread(new Runnable() {
			public void run() {
				for (int i = 0; i < 5; i++) {
					try {
						Thread.sleep(1000);
						System.out.println("Thread 2 is running : " + i + " " + Thread.currentThread());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

		th1.start();
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		th2.start();
	}
}
