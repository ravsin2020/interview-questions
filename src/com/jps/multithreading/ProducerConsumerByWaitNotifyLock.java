package com.jps.multithreading;

import java.util.LinkedList;
import java.util.List;

public class ProducerConsumerByWaitNotifyLock {
public static void main(String[] args) {
	Processor3 processor = new Processor3();
	Thread producerThread= new Thread(()->{
		try {
			processor.producer();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	});
	Thread consumerThread= new Thread(new Runnable(){ 
		public void run()
		{
		try {
			processor.consumer();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		}
		});
	producerThread.start();
	consumerThread.start();
	
}
}

class Processor3{
	List list = new LinkedList();
	private final int LIMIT = 2;
	Object lock= new Object();
	
	
	public void producer() throws InterruptedException{
		int value = 0;
		while(true){
			synchronized(lock){
				if(list.size() == LIMIT){
					lock.wait();
				}
				int tempVal= value++;
				list.add(tempVal);
				System.out.println("Producer Produces : " + tempVal);
				lock.notify();
			}
			Thread.sleep(1000);
		}
		
	}
	public void consumer() throws InterruptedException{
		while(true){
			synchronized(lock){
				if(list.size() == 0){
					lock.wait();
				}
				System.out.println("Consumer consumes : " + list.remove(list.size()-1) );
				lock.notify();
			}
			Thread.sleep(3000);
		}
		
	}
}
