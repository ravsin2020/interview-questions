package com.jps.multithreading;

import java.util.concurrent.locks.ReentrantLock;

public class Pring1to100Using2Thread {
	public static void main(String[] args) {
		Processor5 processor = new Processor5();
		Thread th1= new Thread(new Runnable(){
			@Override
			public void run(){
				try {
					processor.readEvenChar();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		Thread th2= new Thread(new Runnable(){
			@Override
			public void run(){
				try {
					processor.readOddChar();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		th2.start();
		th1.start();
	}

}
class Processor5{
	private static volatile int count=0;
	ReentrantLock obj= new ReentrantLock();
	Processor5(){
	}
	public void readOddChar() throws InterruptedException{
		while(100>count){
			synchronized(obj){
				if(count%2!=0){
					obj.wait();
				}
				System.out.println(Thread.currentThread()+" "+count);
				count+=1;
				obj.notify();
			}
		}
	}
	public void readEvenChar() throws InterruptedException{
		while(100>count){
			synchronized(obj){
				if(count%2==0){
					obj.wait();
				}
				System.out.println(Thread.currentThread()+" "+count);
				count+=1;
				obj.notify();
			}
		}
	}
}
