package com.jps.multithreading;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
public class MyThreadPool {
public static void main(String[] args) {
	ThreadPool2 pool= new ThreadPool2(5);
	for (int i = 0; i < 10; i++) {pool.submit(new Task2());}
	//pool.shutdown();
}}
class ThreadPool2 {
	private final int n;  
	private final WorkerThread[] threads;  
	private final BlockingQueue queue;
	public ThreadPool2(int n) {
		this.n = n; threads = new WorkerThread[n];
		queue = new LinkedBlockingQueue();
		for (int i = 0; i < n; i++) {
			threads[i] = new WorkerThread();
			threads[i].start(); 
			}}
	public void submit(Runnable task) {
		synchronized (queue) {
			queue.add(task);
			queue.notify();}}
	public void shutdown(){
		for (int i = 0; i < n; i++) {
			threads[i].interrupt();
			}}
	class WorkerThread extends Thread {
		public void run() {
			Runnable task;
			while (true) { 
				synchronized (queue) { 
					while (queue.isEmpty()) {
						try { 
							queue.wait(); 
							} catch (InterruptedException e) {
							//System.out.println("An error occurred while queue is waiting: " + e.getMessage());
							Thread.currentThread().interrupt(); return;
						} } 
						task = (Runnable) queue.poll(); } // If we don't catch RuntimeException, // the pool could leak threads
				try { 
					task.run();
					} catch (RuntimeException e) { System.out.println("Thread pool is interrupted due to an issue: " + e.getMessage());
				} } } } }
class Task2 implements Runnable{
	@Override
	public void run() {
		try { Thread.sleep(1000); } catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Task2 is running");
	}
	
}
