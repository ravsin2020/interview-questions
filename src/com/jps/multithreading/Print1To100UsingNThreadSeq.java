package com.jps.multithreading;

public class Print1To100UsingNThreadSeq {
public static void main(String[] args) {
	ThreadProcessor processor= new ThreadProcessor(4);
	Thread th1= new Thread(()->{
		processor.doProcess(1, 100);
	});
	th1.setName("Th1");
	
	Thread th2=new Thread(()->{
		processor.doProcess(2, 100);
	});
	th2.setName("Th2");
	
	Thread th3=new Thread(()->{
		processor.doProcess(3, 100);
	});
	th3.setName("Th3");
	Thread th4=new Thread(()->{
		processor.doProcess(4, 100);
	});
	th4.setName("Th4");

	th1.start();
	th2.start();
	th3.start();
	th4.start();
	
}
}
class ThreadProcessor{
	private final int thCount;
	private volatile int localCount;
	private volatile int count;
	ThreadProcessor(int thCount){
		this.thCount=thCount+1;
		localCount= 1;
		count=1;
	}
	public void doProcess(int threadNo , int max){
		while(count<=max){
			synchronized (this) {
				if(localCount!= threadNo){
					try {
						wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}else if(localCount== threadNo){
					System.out.println(Thread.currentThread().getName()+ " : "+ count);
					/*try {
						//Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					count++;
					localCount++;
					notifyAll();
				}
				if(localCount==thCount){
					localCount=1;
				}
			}
				
		}
		
	}
}
