package com.jps.multithreading;

public class ThreadCreationByLembda {
	public static void main(String[] args) {
		Thread th1=  new Thread(()->{
			for (int i = 0; i < 5; i++) {
				try {
					Thread.sleep(1000);
					System.out.println("Thread 1 i running : " + i + " " +Thread.currentThread());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread th2= new Thread(()->{
			for (int i = 0; i < 5; i++) {
				System.out.println("Thread2 is running : " + i + " " + Thread.currentThread());
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		th1.setName("Hai");
		th1.setPriority(Thread.MAX_PRIORITY);
		th2.setName("Hello");
		th2.setPriority(Thread.MIN_PRIORITY);	
		th1.start();
		th2.start();
		try {
			th1.join();
			th2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(th1.isAlive());
		System.out.println("Main is printing");
		
//		Hai hai =(a,b)->{return a+b;};
//		System.out.println(hai.add(5, 3));
				
				
	}
}
interface Hai{
	
	public int add(int ab, int bc);
}
