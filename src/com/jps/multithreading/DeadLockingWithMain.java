package com.jps.multithreading;

public class DeadLockingWithMain {
public static void main(String[] args) throws InterruptedException {
	System.out.println("Locking main Thread");
	Thread.currentThread().join();
	System.out.println("Finished");
}
}
