package com.jps.multithreading;

public class TestSynchonized {
	
	public static void main(String[] args) {
		Count obj= new Count();
		Thread th1=new Thread(()->{
			for (int i = 0; i <100000; i++) {
				obj.increment();
			}
		});
		Thread th2 = new Thread(()->{
			for(int i= 0; i<100000; i++){
				obj.increment();
			}
		});
		th1.start();
		th2.start();
		try {
			th1.join();
			th2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(obj.count);
	}
}

class Count {
	 int  count;
	public synchronized void increment(){
			count++;
	}
}
