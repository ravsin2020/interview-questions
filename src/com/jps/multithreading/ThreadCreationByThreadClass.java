package com.jps.multithreading;

public class ThreadCreationByThreadClass {
	public static void main(String[] args) {
		HiThread th1 = new HiThread();
		th1.start();
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HelloThread th2 = new HelloThread();
		th2.start();
	}

}

class HiThread extends Thread {

	public void run() {
		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Thread1 is running " + i + Thread.currentThread());
		}
	}
}

class HelloThread extends Thread {

	public void run() {
		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Thread1 is running " + i + Thread.currentThread());
		}
	}
}
