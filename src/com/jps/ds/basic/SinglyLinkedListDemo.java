package com.jps.ds.basic;

/**
 * 
 * @author Ravikant.singh
 *
 *         This class is defined for Custom Singly linked list. we can add,
 *         remove and iterate functionality can perform
 */
public class SinglyLinkedListDemo {
	public static void main(String[] args) {
		SinglyLinkedList list = new SinglyLinkedList<>();
		list.add(10);
		list.add(20);
		list.add(50);
		list.add("Shyam");
		list.add("Hari");
		list.add(90);
		list.iterate();
		System.out.println("**");
		list.removeLast();
		list.iterate();

		System.out.println("***");
		list.removeFirst();
		list.iterate();
	}
}
