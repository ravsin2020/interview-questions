package com.jps.ds;

import java.util.LinkedList;
import java.util.Queue;

public class StackUsingQueue {
public static void main(String[] args) {
	CustomStack stack= new CustomStack();
	stack.push(10);
	stack.push(20);
	stack.push(30);
	stack.push(40);
	stack.push(50);
	System.out.println(stack.top());
	System.out.println(stack.pop());
	
}
}
class CustomStack{
   private Queue<Integer> q;
   CustomStack(){
	   q= new LinkedList<>();   
   }
   public void push(int val){
	   int size= q.size();
	   q.add(val);
	   for(int i=0;i<size;i++){
		   int x= q.remove();
		   q.add(x);
	   }
   }
   public int pop(){
	   if(q.isEmpty()){
		   System.out.println("Stack is Empty");
		   return -1;
	   }
	   return q.remove();
   }
   public int top(){
	   if(q.isEmpty()){
		   System.out.println("Stack is Empty");
		   return -1;
	   }
	   return q.peek();
   }
}
