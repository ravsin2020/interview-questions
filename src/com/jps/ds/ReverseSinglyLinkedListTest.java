package com.jps.ds;

public class ReverseSinglyLinkedListTest {
	public static void main(String[] args) {
		SinglyLinkedList list = new SinglyLinkedList();
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		list.add(60);

		list.iterate();
		list.reverse();
		System.out.println("After reverse");
		list.iterate();

	}
}

class SinglyLinkedList {
	Node head;

	class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}

	public void add(int data) {
		Node new_node = new Node(data);
		if (head == null) {
			head = new_node;
			return;
		}
		Node temp = head;
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = new_node;
	}

	public void iterate() {
		Node temp = head;
		while (temp != null) {
			Node temp1 = temp.next;
			System.out.println(temp.data);
			temp = temp1;
		}
	}

	public void reverse() {
		Node current = head;
		Node next = null;
		Node prev = null;
		while (current != null) {
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}
		head = prev;
	}

}
