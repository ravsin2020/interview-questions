package com.jps.ds;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;
/**
 * 
 * @author Ravikant.singh
 *
 *LRUcache -  The LRU caching scheme is to remove the least recently used frame when the
 * cache is full and a new page is referenced which is not there in cache
 */
public class CustomLRUcacheDemo {
public static void main(String[] args) {
	LRUcache cache= new LRUcache(6);
	cache.add(20);
	cache.add(50);
	cache.add(50);
	cache.add(70);
	cache.add(5);
	cache.add(88);
	cache.add(20);
	cache.add(9);
	cache.add(7);
	cache.add(20);
	cache.iterate();
}
}

class LRUcache {
	private Deque<Integer> dq;
	private Set<Integer> set;
	private final int maxSize;

	public LRUcache(int maxSize) {
		this.maxSize = maxSize;
		dq = new ArrayDeque<>(); // or LinkedList<>(); can be used.
		set = new HashSet<>();
	}

	public void iterate() {
		for (Integer integer : dq) {
			System.out.println(integer);
		}
	}

	public void add(int val) {
		//check if size if full and it doesn't contain val 
		if (set.size() == maxSize && !set.contains(val)) {
			int leastRecent = dq.removeLast(); // remove the leased used data
			set.remove(leastRecent);
		} else if (set.contains(val)) { // if cache contains the value
					// remove element
					dq.remove(val);
					set.remove(val);
		}
		dq.addFirst(val); // adding to 1st index
		set.add(val); // adding to set as well
	}
}
