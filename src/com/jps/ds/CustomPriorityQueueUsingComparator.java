package com.jps.ds;

import java.util.Comparator;
import java.util.PriorityQueue;

public class CustomPriorityQueueUsingComparator {
	public static void main(String[] args) {
		PriorityQueue<Student> pq = new PriorityQueue<Student>(5, new StudentComparator());
		Student student1 = new Student("Ravi", 3.2); 

        pq.add(student1); 
        Student student2 = new Student("Anmol", 3.6); 
                pq.add(student2);          
        Student student3 = new Student("Kouashl", 4.0); 
                pq.add(student3); 
          
        // Printing names of students in priority order,poll() 
        // method is used to access the head element of queue 
        System.out.println("Students served in their priority order"); 
          
        while (!pq.isEmpty()) { 
        System.out.println(pq.poll().getName()); 
}  
	}

}

class Student {
	public String name;
	public double cgpa;

	// A parameterized student constructor
	public Student(String name, double cgpa) {

		this.name = name;
		this.cgpa = cgpa;
	}

	public String getName() {
		return name;
	}
}

class StudentComparator implements Comparator<Student> {

	// Overriding compare()method of Comparator
	// for descending order of cgpa
	public int compare(Student s1, Student s2) {
		if (s1.cgpa < s2.cgpa)
			return 1;
		else if (s1.cgpa > s2.cgpa)
			return -1;
		return 0;
	}
}
