package com.jps.ds;

public class SinglyLinkedListPalindrom {
	public static void main(String[] args) {
		SinglyLinkedList2 list= new SinglyLinkedList2();
		list.add(1);
		list.add(2);
		list.add(2);
		list.add(1);
		list.checkPalindrom();
	}
}

class SinglyLinkedList2 {
	Node head;

	class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}

	public void add(int data) {
		Node new_node = new Node(data);
		if (head == null) {
			head = new_node;
			return;
		}
		Node temp = head;
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = new_node;
	}

	public void checkPalindrom() {
		Node node1 = head;
		Node node2 = head;
		Node node3 = null;
		while (node2 != null) {
			// Add first hasf in reverse order
			Node node = new Node(node1.data);
			if (node3 == null) {
				node3 = node;
			} else {
				node.next = node3;
				node3 = node;
			}
			node1 = node1.next;
			node2 = node2.next.next;
		}
		boolean isPalidrom = false;
		while (node3 != null) {
			if (node3.data == node1.data) {
				isPalidrom = true;
			} else {
				isPalidrom = false;
			}
			node1=node1.next;
			node3= node3.next;
		}
		System.out.println(isPalidrom);
	}

	public void iterate() {
		Node temp = head;
		while (temp != null) {
			Node temp1 = temp.next;
			System.out.println(temp.data);
			temp = temp1;
		}
	}
}