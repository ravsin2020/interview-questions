package com.jps.ds;

public class QueueUsingSinglyLinkedListTest {
public static void main(String[] args) {
	CustomQueue queue= new CustomQueue();
	queue.enQueue(10);
	queue.enQueue(20);
	queue.enQueue(30);
	queue.enQueue(40);
	queue.enQueue(50);
	queue.enQueue(60);
	
	queue.deQueue();
	
}
}
class CustomQueue{
	Node head;
	class Node{
		int data;
		Node next;
		Node(int data){
			this.data=data;
			next=null;
		}
	}
	 public void enQueue(int data){
		 Node new_node=new Node(data);
		 if(head==null){
			 head=new_node;
			 return;
		 }
		 Node temp=head;
		 while(temp.next!=null){
			 temp=temp.next;
		 }
		 temp.next=new_node;
	 }
	 public void deQueue(){
		 while(head!=null){
			 Node temp=head.next;
			 System.out.println(head.data);
			 head=temp;
		 }
	 }
	  
}
