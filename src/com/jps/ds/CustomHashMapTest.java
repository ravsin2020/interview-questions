package com.jps.ds;

public class CustomHashMapTest {
	public static void main(String[] args) {
		CustomHashMap<Integer, Integer> map = new CustomHashMap<>();
		map.put(10, 100);
		map.put(10, 200);
		map.put(20, 300);
		System.out.println(map.get(20));
		System.out.println("Map size is - " + map.getSize());
	}
}

class CustomHashMap<K, V> {
	private Entry<K, V>[] tab = null;
	private volatile int  size;

	class Entry<K, V> {
		private K key;
		private V value;
		Entry<K, V> next;

		Entry(K key, V value) {
			this.key = key;
			this.value = value;
			next = null;
		}
	}

	CustomHashMap() {
		tab = new Entry[16];
	}

	public void put(K key, V val) {
		int index = getIndex(key);
		Entry<K,V> entryObj = tab[index];
		Entry<K, V> newObj = new Entry<>(key, val);
		if (entryObj == null) {
			tab[index] = newObj;
			size++;
		} else {
			while (entryObj != null) {
				if (entryObj.key.hashCode() == key.hashCode() && entryObj.key.equals(key)) {
					entryObj.value = val;
					return;
				} else {
					entryObj = entryObj.next;
				}
			}
			entryObj.next = newObj;
			size++;
		}
	}

	public int getHashCode(K key) {
		return key.hashCode();
	}

	public int getIndex(K key) {
		return key == null ? 0 : getHashCode(key) % tab.length;
	}

	public V get(K key) {
		if (key == null) {
			return tab[0].value;
		}
		int index = getIndex(key);
		Entry<K, V> currObj = tab[index];
		while (currObj != null) {
			if (currObj.key.hashCode() == key.hashCode() && currObj.key.equals(key)) {
				return currObj.value;

			} else {
				currObj = currObj.next;
			}
		}
		return null;
	}
	public int getSize(){
		return this.size;
	}

}