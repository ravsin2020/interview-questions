package com.jps.ds;
	/**
	 * 
	 * @author Ravikant.singh
	 * 
	 * This class is to find Nth node from Last of Singly linked List
	 *
	 */
public class FindNthFromLast {
	public static void main(String[] args) {
		LinkedList<Integer> list = new LinkedList<>();
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		list.add(60);
		list.findNthFromLast(2);

	}

	private static class LinkedList<T> {
		private Node<T> head;

		LinkedList() {
			head = null;
		}

		public void add(T data) {
			Node<T> newNode = new Node<>(data);
			if (head == null) {
				head = newNode;
			} else {
				Node<T> temp = head;
				while (temp.next != null) {
					temp = temp.next;
				}
				temp.next = newNode;
			}
		}

		public void iterate() {
			if (head != null) {
				Node<T> temp = head;
				while (temp != null) {
					System.out.println(temp.data);
					temp = temp.next;
				}
			}
		}

		public void findNthFromLast(int count) {
			if (head != null) {
				Node<T> slow = head;
				Node<T> fast = head;
				while (count >= 0 && fast != null) {
					if (count > 0) {
						fast = fast.next;
						count--;
						continue;
					}
					slow = slow.next;
					fast = fast.next;
				}
				if (count == 0) {
					System.out.println("Nth from last : " + slow.data);
				} else {
					System.out.println("Count is more then length");
				}
			}
		}
	}

	private static class Node<T> {
		private T data;
		private Node<T> next;

		Node(T data) {
			this.data = data;
			this.next = null;
		}
	}
}
