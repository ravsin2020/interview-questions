package com.jps.ds;

public class StackUsingSinglyLinkedListTest {
	public static void main(String[] args) {
		CustomStackSinglyLinkedList list = new CustomStackSinglyLinkedList();
		list.push(10);
		list.push(20);
		list.push(30);
		list.iterate();

	}
}

class CustomStackSinglyLinkedList {
	Node head;

	class Node {
		int data;
		Node next;

		Node(int input) {
			this.data = input;
			next = null;
		}
	}

	public void push(int data) {
		Node new_node = new Node(data);
		if (head == null) {
			head = new_node;
		} else {
			new_node.next=head;
			head= new_node;
		}
	}

	public void iterate() {
		Node temp = head;
		while (temp != null) {
			Node next_node = temp.next;
			System.out.println(temp.data);
			temp = next_node;
		}
	}

}
