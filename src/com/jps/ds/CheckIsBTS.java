package com.jps.ds;

public class CheckIsBTS {
	public static void main(String[] args) {
		BinarySearchTree1 bts= new BinarySearchTree1();
		bts.add(4);
		bts.add(2);
		bts.add(5);
		bts.add(1);
		bts.add(3);
		System.out.println(bts.isBTS());
		
		System.out.println(bts.searchElement(bts.root,4));
	}

}

class BinarySearchTree1 {
	int data;
	Node leftNode;
	Node rightNode;
	Node root;

	public BinarySearchTree1() {
		root = null;
	}

	class Node {
		int data;
		Node leftNode;
		Node rightNode;

		Node(int data) {
			this.data = data;
			leftNode = null;
			rightNode = null;
		}
	}

	public void add(int key) {
		root = add(key, root);
	}

	public Node add(int key, Node root) {
		if (root == null) {
			Node node = new Node(key);
			root = node;
		}
		if (root.data > key) {
			root.leftNode = add(key, root.leftNode);
		} else if (root.data < key) {
			root.rightNode = add(key, root.rightNode);
		}
		return root;
	}

	public boolean isBTS() {
		Node node=root;
		return isBTSUtil(node, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	public boolean isBTSUtil(Node node, int min, int max) {
		       
		/* an empty tree is BST */
		if (node == null) {
			return true;
		}
		 /* false if this node violates the min/max constraints */
		if(node.data<min || node.data>max){
			return false;
		}
		/* otherwise check the subtrees recursively tightening the min/max constraints */
		return (isBTSUtil(node.leftNode, min, node.data-1) && 
				isBTSUtil(node.rightNode, node.data+1, max));
	}
	public Node searchElement(int key){
		Node node=root;
		return searchElement(node, key);
	}
	//Search element
	public Node searchElement(Node node, int key){
		if(node==null || node.data==key){
			return node;
		}else if(node.data > key){
			return searchElement(node.leftNode, key);
		}else//(node.data < key)
			{
			return searchElement(node.rightNode, key);
		}
//		else{
//			return node;
//		}
	}
}
