package com.jps.ds;

public class PalindromCheckSinglyLinkedList2<T> {
	Node<T> head;
	public static void main(String[] args) {
		PalindromCheckSinglyLinkedList2<Integer> list= new PalindromCheckSinglyLinkedList2<Integer>();
		list.addElement(10);
		list.addElement(20);
		list.addElement(30);
		list.addElement(40);
		System.out.println("Traversing");
		list.traverse();
		System.out.println("");
		System.out.println("Reversing");
		list.reverse();
		list.traverse();
	}
	public void addElement(T element){
		Node<T> nd= new Node<T>();
		nd.value = element;
		Node<T> temp = head;
		while(true){
			if(temp == null){
				//since there is only one element so, bothe head and tail pointe same element
				head = nd;
				break;
			}else
			if(temp.next == null){
				temp.setNextRef(nd);
				break;
			}else{
				temp = temp.next;
			}
		}
		
	}
	public void traverse(){
		Node<T> temp = head;
		while(true){
			if(temp == null){
				break;
			}
			System.out.print(temp.getValue()+ "\t");
			temp = temp.getNextRef();
		}
	}
	public void reverse(){
		Node<T> current= head;
		Node<T> prev = null;
		Node<T> next = null;
		while(current != null){
			next = current.getNextRef();
			current.setNextRef(prev);
			prev= current;
			current = next;
		}
		 head = prev;
	}

class Node<T> implements Comparable<T> {
	T value;
	Node<T> next;

	public void setValue(T value) {
		this.value = value;
	}

	public T getValue() {
		return this.value;
	}

	public void setNextRef(Node<T> nextRef) {
		this.next = nextRef;
	}

	public Node<T> getNextRef() {
		return this.next;
	}

	@Override
	public int compareTo(T arg) {
		if (arg == value) {
			return 0;
		} else
			return 1;
	}

}
}

