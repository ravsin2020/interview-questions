package com.jps.ds;

public class StackTest1 {
	public static void main(String[] args){
		Stack1 stack= new Stack1();
		try {
			stack.push(10);
			stack.push(20);
			stack.push(30);
			stack.push(40);
			
			System.out.println(stack.top);
			
			System.out.println("Element removed : " + stack.pop());
			System.out.println("Element removed : " + stack.pop());
			System.out.println("Element removed : " + stack.pop());
			System.out.println("Element removed : " + stack.pop());
			System.out.println("Element removed : " + stack.pop());
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
}

class Stack1 {
	public static final int MAX = 100;
	int[] s = new int[MAX];
	int top;

	Stack1() {
		top = -1;
	}

	public boolean push(int data) throws StackOverFlowException {
		if (top +2 > MAX) {
			throw new StackOverFlowException("Stack over flow - top > max");
		}
		{
			s[++top] = data;
			return true;
		}
	}
	public int pop() throws StackOverFlowException {
		if (top < 0) {
			throw new StackOverFlowException("Stack over flow - top < 0");
		} else {
			int x = s[top--];
			s[top + 1]=0;
			return x;
		}
	}
}