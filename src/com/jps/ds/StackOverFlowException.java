package com.jps.ds;

public final class StackOverFlowException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3244810903312655434L;

	StackOverFlowException(){
	}
	StackOverFlowException(String msg){
		super(msg);
	}
}
