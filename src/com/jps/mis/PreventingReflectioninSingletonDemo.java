package com.jps.mis;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class PreventingReflectioninSingletonDemo {
	public static void main(String[] args)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
				
		Singleton instance1 = Singleton.getSingletonInstance();
		Singleton instance2 = Singleton.getSingletonInstance();
		System.out.println(instance1 + " , " + instance2);
		System.out.println("*****Breaking Singleton using Refleation*****");
		Constructor constructor = Singleton.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		Singleton instance3 = (Singleton) constructor.newInstance();
		System.out.println("New instance using Singleton : " + instance3 );

	}
}

class Singleton {
	private static volatile Singleton instance;

	private Singleton() {
		// the below check will prevent instantiation 
		if(instance!=null){
			throw new InstantiationError("Creating new instance not allowed");
		}
	}

	public static Singleton getSingletonInstance() {
		if (instance == null) {
			synchronized (Singleton.class) {
				if (instance == null) {
					instance = new Singleton();
					return instance;
				}
			}
		}
		return instance;
	}
}
