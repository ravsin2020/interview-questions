package com.jps.mis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test9 {
	public static void main(String[] args) {
		Map<String, List<int[]>> map = new HashMap<>();
		List<int[]> list = new ArrayList<>();
		list.add(new int[] { 1, 0, 1 }); // E=1,D=0,V=1
		list.add(new int[] { 1, 1, 0 });
		// ...
		map.put("RJL", list); // similarly other client
		// ..

	}
}
