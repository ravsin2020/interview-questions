package com.jps.systemDesign;
import java.util.HashMap;

import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
/**
 * 
 * @author Ravikant.Singh
 *
 */
public class TrafficSignal {
	public static Map<String, String> signal = new HashMap<String, String>();
	final Semaphore s = new Semaphore(1, true);
	private class Task extends Thread {
		private String task;
		public Task(String task) {super();this.task = task;		}
		@Override
		public void run() {
			while (true) {try {s.acquire();TimeUnit.SECONDS.sleep(3);
				} catch (InterruptedException e) {e.printStackTrace();} 
			finally {
					s.release();}}}}
	public Task newTask(String task) {
		return new Task(task);
	}
	public static void main(String[] args) {
		signal.put("TIMED_WAITING", "GREEN");signal.put("WAITING", "RED");signal.put("RUNNABLE", "RED");signal.put("RUNNABLE", "YELLOW");
		TrafficSignal tSignal= new TrafficSignal();Task t1= tSignal.newTask("S->N");Task t2= tSignal.newTask("W->E");
		Task t3= tSignal.newTask("N->S");Task t4= tSignal.newTask("E->W");
		Thread monitor= new Thread(new Runnable() {
			@Override
			public void run() {
				while(true){
					System.out.println(t1.task + ": " + signal.get(t1.getState().toString()) + "  "+ t2.task + ": " + signal.get(t2.getState().toString()) + "  "
										+ t3.task + ": " + signal.get(t3.getState().toString()) + "  "+ t4.task + ": " + signal.get(t4.getState().toString()) + "  ");
					try {TimeUnit.SECONDS.sleep(3);
					} catch (InterruptedException e) {e.printStackTrace();}}}
		});
		monitor.start();
		t1.start();
		t2.start();
		t3.start();
		t4.start();
	}

}
