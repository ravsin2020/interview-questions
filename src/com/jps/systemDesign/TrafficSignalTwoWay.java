package com.jps.systemDesign;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
/**
 * 
 * @author Ravikant.Singh
 *
 */
public class TrafficSignalTwoWay {
	final Semaphore s = new Semaphore(1, true);
	private class Task extends Thread {
		private String task;
		public Task(String task) {
			super();
			this.task = task;
		}
		@Override
		public void run() {
			while (true) {
				try {
					s.acquire();
					System.out.println(Thread.currentThread());
					TimeUnit.SECONDS.sleep(3);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					s.release();
				}
			}
		}
	}
	public Task newTask(String task) {
		return new Task(task);
	}
	public static void main(String[] args) {
		TrafficSignalTwoWay tSignal= new TrafficSignalTwoWay();
		Task t1= tSignal.newTask("S->N");
		t1.setName("S-N");
		Task t2= tSignal.newTask("N->S");
		t2.setName("N-S");
		t1.start();
		t2.start();
		
	}

}
