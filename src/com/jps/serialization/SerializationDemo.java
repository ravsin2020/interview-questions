package com.jps.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationDemo {
	public static void main(String[] args) {
		User user= getUserObject();
		System.out.println(user.toString());
		try {
			FileOutputStream fos= new FileOutputStream("C:/Users/Ravi/Desktop/File IO,Serialization, Excaption Handling & Logging/source/SerDemo.text");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(user);
			fos.close();
			oos.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static User getUserObject(){
		User user= new User(10001);
		user.uid=900001;
		user.name="Raj";
		user.eid=5768;
		user.cname="Accolite";
		user.permAddress= new Address("Pune", 892367);
		user.compAddress= new Address("Bangalore", 560087);
		user.account="10001001";
		return user;
	}
}
